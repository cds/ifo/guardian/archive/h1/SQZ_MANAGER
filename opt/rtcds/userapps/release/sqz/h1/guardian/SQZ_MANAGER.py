# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LOCK_LR.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_LO_LR.py $

from guardian import GuardState, GuardStateDecorator, NodeManager

import ISC_GEN_STATES
import ISC_library

import time, os, sys, datetime, gpstime
import cdsutils as cdu
import json
from timeout_utils import call_with_timeout

import numpy as np
from matplotlib import pyplot as plt

import sqzparams


##################################################
# Prep file path for saving offsets
##################################################
ZMslidersPath = '/opt/rtcds/userapps/trunk/sqz/h1/guardian/ZM_sliders.dat'
with open(ZMslidersPath, 'r') as ZMsParamFile:
    ZMSlidersDict = json.load(ZMsParamFile)

##################################################
# NODES
##################################################

nodes = NodeManager(['SQZ_CLF_LR',
                     'SQZ_OPO_LR',
                     'SQZ_LO_LR',
                     'SQZ_SHG',
                     'SQZ_FC',
                     'SQZ_ANG_ADJUST',
                     'SQZ_PMC'
                     ])

nominal = 'FREQ_DEP_SQZ'

use_ifo_asc = sqzparams.use_ifo_as42_asc
use_sqz_angle_adjust = sqzparams.use_sqz_angle_adjust

##################################################
# FUNCTIONS
##################################################
def OPO_LOCKED_CLF():
    flag = False
    if ezca['GRD-SQZ_OPO_LR_STATE_S'] == 'LOCKED_CLF_DUAL':
        flag = True
    if ezca['GRD-SQZ_OPO_LR_STATE_N'] >= 17:
        flag = True
    return flag

def PMC_locked():
    if ezca['GRD-SQZ_PMC_STATE_S'] == 'LOCKED':
        return True

def SHG_locked():
    if ezca['GRD-SQZ_SHG_STATE_S'] == 'LOCKED':
        return True

def SHG_PZT_OK():
    return 15 < ezca['SQZ-SHG_PZT_VOLTS'] < 85

def OPO_PZT_OK():
    return 50 < ezca['SQZ-OPO_PZT_1_VOLTS'] < 110

def PMC_PZT_OK():
    return 5 < ezca['SQZ-PMC_PZT_VOLTS'] < 95

def CLF_LOCKED():
    flag = False
    if ezca.read('GRD-SQZ_CLF_LR_STATE_S', as_string=True) == 'LOCKED':
        flag = True
    return flag

def EOM_OK():
    return abs(ezca['SQZ-FIBR_SERVO_EOMRMSMON']) < 4

def TTFSS_LOCKED():
    flag = False
    if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
        flag = True
    return flag

def LO_LOCKED_HD():
    flag = False
    if ezca.read('GRD-SQZ_LO_LR_STATE_S', as_string=True) == 'LOCKED_HD':
        if abs(ezca['SQZ-LO_SERVO_FASTMON'] < 9):
            flag = True
        else:
            notify('LO railing!')
    return flag

def LO_LOCKED_OMC():
    flag = False
    if ezca.read('GRD-SQZ_LO_LR_STATE_S', as_string=True) == 'LOCKED_OMC':
        if abs(ezca['SQZ-LO_SERVO_FASTMON'] < 9):
            flag = True
        else:
            notify('LO railing!')
    return flag

def FC_LOCKED():
    flag = False
    if nodes['SQZ_FC'] == 'IR_LOCKED':
        flag = True
    elif nodes['SQZ_FC'] == 'FC_MISALIGNED':
        flag = True
    return flag

def turn_off_sqz():
    ezca['SQZ-ASC_WFS_SWITCH'] = 0
    nodes['SQZ_LO_LR'] = 'DOWN'
    nodes['SQZ_ANG_ADJUST'] = 'DOWN'
    if ezca['SYS-MOTION_C_BDIV_E_OPENSWITCH']:
        ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1
        time.sleep(1)
    if ezca['SYS-MOTION_C_BDIV_E_BUSY']:
        notify('Waiting for Beam Diverter')
        time.sleep(6)

def turn_off_fc():
    if not ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'FC_MISALIGNED' or 'DOWN':
        nodes['SQZ_FC'] = 'DOWN'

def turn_off_asc():
    log('Turning off SQZ ASC before running scan sqz align or scan sqz angle.')
    ezca['SQZ-ASC_WFS_SWITCH'] = 0
    time.sleep(2)
    for dof in ['POS_P','ANG_P','POS_Y','ANG_Y']:
        ezca['SQZ-ASC_%s_RSET'%(dof)] = 2

def IFO_LOCKED():
    flag = False
    if ezca['GRD-ISC_LOCK_STATE_N'] > 500 or ezca['GRD-OMC_LOCK_STATE_N'] > 400:
      flag = True
    return flag

def PSL_OK():
    return ezca['PSL-FSS_AUTOLOCK_STATE'] == 4

### making some decorators
class PMC_checker(GuardStateDecorator):
    def pre_exec(self):
        if not PMC_locked():
            log('PMC unlocked')
            return 'LOCK_PMC'

class SHG_checker(GuardStateDecorator):
    def pre_exec(self):
        if not SHG_locked():
            log('SHG unlocked')
            return 'LOCK_SHG'

class PZT_checker(GuardStateDecorator):
    def pre_exec(self):
        if not SHG_PZT_OK():
            log('SHG PZT Volts close to railing, relocking SHG.')
            return 'LOCK_SHG'
        if not OPO_PZT_OK():
            log('OPO PZT Volts close to railing, relocking OPO.')
            return 'LOCK_OPO'
        if not PMC_PZT_OK():
            log('PMC PZT Volts close to railing, relocking PMC.')
            return 'LOCK_PMC'

class OPO_checker(GuardStateDecorator):
    def pre_exec(self):
        if not OPO_LOCKED_CLF():
            return 'LOCK_OPO'

class LO_checker_FDS(GuardStateDecorator):
    def pre_exec(self):
        if not LO_LOCKED_OMC():
            log('LO unlocked')
            return 'FDS_READY_IFO'

class TTFSS_checker(GuardStateDecorator):
    def pre_exec(self):
        if not TTFSS_LOCKED():
            log('TTFSS unlocked')
            #turn_off_sqz() #vx 1/12
            return 'LOCK_TTFSS'
        elif not EOM_OK():
            log('TTFSS EOM noisy')
            #turn_off_sqz() #vx 1/12
            return 'LOCK_TTFSS'

class FC_checker(GuardStateDecorator):
    def pre_exec(self):
        if not FC_LOCKED():
            log('FC unlocked')
            return 'FC_WAIT_FDS'

class IFO_checker(GuardStateDecorator):
    def pre_exec(self):
        if not IFO_LOCKED():
            log('IFO not at DC readout')
            return 'FDS_READY_IFO'

class LO_OMC_checker(GuardStateDecorator):
    def pre_exec(self):
        if not LO_LOCKED_OMC():
            log('LO OMC unlocked!')
            return 'FDS_READY_IFO'


def IMC_LOCKED():
    log('Checking IMC in IMC_LOCKED()')
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] == 100 or ezca['GRD-IMC_LOCK_STATE_N'] == 70:
        flag = True
    return flag



#############################################
#States
#############################################

class INIT(GuardState):
    index = 0

    def main(self):
         nodes.set_managed()
         return True


class DOWN(GuardState):
    index = 5
    goto = True
    request = True

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        self.counter = 0
        self.timer['pause'] = 1

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if ezca['SYS-MOTION_C_BDIV_E_CLOSEDSWITCH'] == 0:
            log('SQZ beam diverter is open')
            #turn_off_sqz()

        elif self.counter == 0:
            nodes['SQZ_CLF_LR'] = 'DOWN'
            nodes['SQZ_LO_LR'] = 'DOWN'
            nodes['SQZ_OPO_LR'] = 'DOWN'
            if False:
            #temporary test of unlocking all sqz loops in down so that we can see if this has any impact on the IFO glitching.  Dec 13th 2024
                nodes['SQZ_SHG'] = 'DOWN'
                nodes['SQZ_PMC'] = 'DOWN'
                #ezca['SQZ-FIBR_LOCK_LOGIC_ENABLE'] = 0
            ezca['SQZ-SEED_PZT_SCAN_ENABLE'] = 0
            if not ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'FC_MISALIGNED':
                nodes['SQZ_FC'] = 'DOWN'
            if self.timer['pause']:
                self.counter += 1

        elif self.counter == 1:
            if not PSL_OK():
                notify('Wating in DOWN for PSL FSS to be locked.')
                return False
            else:
                return True


class LOCK_TTFSS(GuardState):
    index = 6
    goto = False
    request = False
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        self.timer['check_ttfss'] = 100

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if TTFSS_LOCKED():
            log('ttfss locked')
            if EOM_OK():
                log('eom looks good')
                return True
            else:
                notify('TTFSS EOM noisy')

        else:
            notify('Waiting for TTFSS to lock, check Crystal Frequency (can try -45 MHz).')
            if self.timer['check_ttfss']:
                if IMC_LOCKED() and not ezca['SQZ-FIBR_LOCK_STATUS_LOCKED']:
                    log('IMC is locked, toggle re-enable TTFSS autolocker')
                    ezca['SQZ-FIBR_LOCK_LOGIC_ENABLE'] = 0
                    time.sleep(5)
                    ezca['SQZ-FIBR_LOCK_LOGIC_ENABLE'] = 1
                    time.sleep(5)
                else:
                    log('imc not locked? waiting to re-enage ttfss')
                self.timer['check_ttfss'] = 200


class LOCK_PMC(GuardState):
    index = 10
    request = False
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        turn_off_fc()

        if not PMC_PZT_OK():
            log('PMC PZT Volts low, relocking PMC.')
            nodes['SQZ_PMC'] = 'DOWN'
            time.sleep(1)
        nodes['SQZ_PMC'] = 'LOCKED'
        time.sleep(1)

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_PMC'].arrived and nodes['SQZ_PMC'].done:
            return True


class LOCK_SHG(GuardState):
    index = 20
    request = False
    @PMC_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        turn_off_fc()

        if not SHG_PZT_OK():
            log('SHG PZT Volts low, relocking SHG.')
            nodes['SQZ_SHG'] = 'DOWN'
            time.sleep(1)
        nodes['SQZ_SHG'] = 'LOCKED'
        time.sleep(1)

    @PMC_checker
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_SHG'].arrived and nodes['SQZ_SHG'].done and nodes['SQZ_SHG']=='LOCKED':
            return True


class LOCK_OPO(GuardState):
    index = 28
    request = False
    @SHG_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        turn_off_fc()

        # lock OPO
        if not OPO_PZT_OK():
            log('OPO PZT Volts close to railing, relocking OPO.')
            nodes['SQZ_OPO_LR'] = 'DOWN'
            time.sleep(1)
        nodes['SQZ_OPO_LR'] = 'LOCKED_CLF_DUAL'
        time.sleep(1)


    @SHG_checker
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO_LR'].arrived and nodes['SQZ_OPO_LR'].done and nodes['SQZ_OPO_LR']=='LOCKED_CLF_DUAL':
            return True


class LOCK_CLF(GuardState):
    index = 29
    request = False

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_fc()
        nodes['SQZ_CLF_LR'] = 'LOCKED'
        self.reset = 0
        self.timer['pause'] = 5

    @TTFSS_checker
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_CLF_LR'].arrived and nodes['SQZ_CLF_LR'].done and not self.reset:
            return True
        elif self.timer['pause']:#if 5 seconds has gone by
            if not self.reset:   #if we aren't in the middle of a reset, try reseting CLF request to down
                self.reset = 1
                nodes['SQZ_CLF_LR'] = 'DOWN'
                self.timer['pause'] = 5
            else:  #if we have requested a reset
                self.reset = 0
                nodes['SQZ_CLF_LR'] = 'LOCKED'
                self.timer['pause'] = 5


class LOCK_FC(GuardState):
    index = 30
    request = False
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        ezca['SQZ-FC_FIBR_FLIPPER_CONTROL']=1
        nodes['SQZ_FC'] = 'GR_LOCKED'

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_FC'].arrived and nodes['SQZ_FC'].done:
            return True


class FDS_READY_IFO(GuardState):
    index = 50

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        nodes['SQZ_FC'] = 'IR_FOUND'
        nodes['SQZ_CLF_LR'] = 'LOCKED'
        if use_ifo_asc:
            log('will run SQZ ASC')
        else:
            log('will not run SQZ ASC')
        self.counter = 0

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @PZT_checker
    def run(self):
        log(self.counter)
        if not PSL_OK():
            notify('PSL FSS unlocked?')
            time.sleep(5)
            #if not PSL_OK():
            #    return 'DOWN'

        if ezca.read('GRD-SQZ_MANAGER_REQUEST_S', as_string=True) == 'NO_SQUEEZING':
            return True

        if self.counter == 0:
            if (nodes['SQZ_LO_LR'].arrived and nodes['SQZ_LO_LR'].done):
                if nodes['SQZ_FC'].arrived and nodes['SQZ_FC'].done:
                    #SED note, Vicky says the reason for this is that the IR drifts away while we are waiting for the IFO to lock, so we have to re-request it to do the IR search right before we open the beam diverter.  I think this could be written in a less confusing way by waiting for the IFO to be locked before we do the search in the first place, but I'm leaving this alone for now.
                    if ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'IR_FOUND':
                        log('FC at IR_FOUND')
                        nodes['SQZ_FC'] = 'GR_LOCKED'
                        log('SQZ Ready, waiting for IFO!')
                        self.counter += 1

        elif self.counter == 1:
            return True

class FIS_READY_IFO(GuardState):
    index = 51
    request = False
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        nodes['SQZ_FC'] = 'FC_MISALIGNED'
        nodes['SQZ_CLF_LR'] = 'LOCKED'
        if use_ifo_asc:
            log('will run SQZ ASC')
        else:
            log('will not run SQZ ASC')
        self.counter = 0

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @PZT_checker
    def run(self):
        log(self.counter)
        if not PSL_OK():
            notify('PSL FSS unlocked?')
            time.sleep(5)
            #if not PSL_OK():
            #    return 'DOWN'

        if ezca.read('GRD-SQZ_MANAGER_REQUEST_S', as_string=True) == 'NO_SQUEEZING':
            return True

        if self.counter == 0:
            #why are we checking LO guardian here?
            if (nodes['SQZ_LO_LR'].arrived and nodes['SQZ_LO_LR'].done):
                if nodes['SQZ_FC'].arrived and nodes['SQZ_FC'].done:
                    notify('FC misaligned')
                    self.counter += 1

        elif self.counter == 1:
            if not IFO_LOCKED():
                log('SQZ Ready, waiting for IFO!')
            return True

class BEAMDIV_OPEN_FDS(GuardState):
    index = 55
    request = False

    # in this state wait for everything to be locked! but don't close the beam diverter
    # idea of this state is that we don't have to unlock everything
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        self.timer['bdiv_timeout'] = 180
        if not IFO_LOCKED():
            return False
        nodes['SQZ_FC'] = 'IR_FOUND'

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    def run(self):

        if self.timer['bdiv_timeout']:
            return 'FDS_READY_IFO'

        elif ezca['SYS-MOTION_C_BDIV_E_BUSY']:
            notify('Waiting for Beam Diverter')
            return False

        elif ezca['SYS-MOTION_C_BDIV_E_OPENSWITCH']:
            log('BDiv open')
            if TTFSS_LOCKED() and CLF_LOCKED() and nodes['SQZ_FC'].arrived and nodes['SQZ_FC'].done:
                log('Everything ready')
                return True
            else:
                nodes['SQZ_LO_LR'] = 'DOWN'
                notify('LO staying in DOWN until FC, TTFSS and CLF are locked.')

        elif not CLF_LOCKED():
            log('CLF, unlocked, Close bdiv while clf relocks')
            return 'FDS_READY_IFO'

        else:
            if nodes['SQZ_FC'].arrived and nodes['SQZ_FC'].done:
                log('Opening Beam Diverter')
                ezca['SYS-MOTION_C_BDIV_E_OPEN'] = 1
                time.sleep(0.5)
            else:
                notify('Waiting for FC to open bdiverter')

class BEAMDIV_OPEN_FIS(GuardState):
    index = 56
    request = False

    # in this state wait for everything to be locked! but don't close the beam diverter
    # idea of this state is that we don't have to unlock everything
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        self.timer['bdiv_timeout'] = 180
        if not IFO_LOCKED():
            return False

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    def run(self):

        if self.timer['bdiv_timeout']:
            return 'FIS_READY_IFO'

        elif ezca['SYS-MOTION_C_BDIV_E_BUSY']:
            notify('Waiting for Beam Diverter')
            return False

        elif ezca['SYS-MOTION_C_BDIV_E_OPENSWITCH']:
            log('BDiv open')
            if TTFSS_LOCKED() and CLF_LOCKED():
                log('Everything ready')
                return True
            else:
                nodes['SQZ_LO_LR'] = 'DOWN'
                notify('LO staying in DOWN until FC, TTFSS and CLF are locked.')

        elif not CLF_LOCKED():
            log('CLF, unlocked, Close bdiv while clf relocks')
            return 'FIS_READY_IFO'

        else:
            log('Opening Beam Diverter')
            ezca['SYS-MOTION_C_BDIV_E_OPEN'] = 1
            time.sleep(0.5)


#-------------------------------------------------------------------------------
#this state is used both for FIS and FDS, this allows us to use the same code to make two distinct states in the graph for a FIS and an FDS branch
def gen_LOCK_LO(nodes):
    class LOCK_LO(GuardState):
        request = False

        @OPO_checker
        @ISC_library.unstall_nodes(nodes)
        def main(self):
            nodes['SQZ_LO_LR'] = 'LOCKED_OMC'
            log('Request SQZ_LO_LR locked OMC')

        @OPO_checker
        @ISC_library.unstall_nodes(nodes)
        def run(self):
            if LO_LOCKED_OMC() and nodes['SQZ_LO_LR'].arrived:
                log('LO locked on OMC_RF3.')
                return True
    return LOCK_LO

LOCK_LO_FDS = gen_LOCK_LO(nodes)
LOCK_LO_FDS.index = 60

LOCK_LO_FIS = gen_LOCK_LO(nodes)
LOCK_LO_FIS.index = 61


#-------------------------------------------------------------------------------
def gen_SQZ_ASC(nodes):
    class SQZ_ASC(GuardState):
        request = False
        @OPO_checker
        @ISC_library.unstall_nodes(nodes)
        def main(self):
            if use_ifo_asc:
                ezca['SQZ-ASC_WFS_SWITCH'] = 1
            self.timer['pause'] = 2

        @OPO_checker
        @ISC_library.unstall_nodes(nodes)
        def run(self):
            if use_ifo_asc and self.timer['pause']:
                if ezca['SQZ-ASC_ANG_P_INMON']!=0 and ezca['SQZ-ASC_ANG_Y_INMON']!=0:
                    log('engaged SQZ ASC')
                    return True
            if not use_ifo_asc:
                log('not running SQZ ASC')
                return True
    return SQZ_ASC
SQZ_ASC_FDS = gen_SQZ_ASC(nodes)
SQZ_ASC_FDS.index = 65
SQZ_ASC_FIS = gen_SQZ_ASC(nodes)
SQZ_ASC_FIS.index = 66

#-------------------------------------------------------------------------------
class SQZ_ANG_SERVO(GuardState):
    index = 69
    request = False

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    @LO_checker_FDS
    def main(self):
        if use_sqz_angle_adjust:
            nodes['SQZ_ANG_ADJUST'] = 'ADJUST_SQZ_ANG_ADF'

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    @LO_checker_FDS
    def run(self):
        if use_sqz_angle_adjust:
            if nodes['SQZ_ANG_ADJUST'].arrived and nodes['SQZ_ANG_ADJUST'].done:     
                return True
        else:
            return True

#-------------------------------------------------------------------------------
class MISALIGN_FC(GuardState):
    index = 110
    request = False
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        log('going into FIS')
        nodes['SQZ_FC'] = 'FC_MISALIGNED'
        self.timer['wait_for_lo'] = 30
    
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    def run(self):
        log(self.counter)
        if self.timer['wait_for_lo']:
        #after FC has been misaligned for 30 seconds, check if the LO is locked or not
            if not LO_LOCKED_OMC():
                log('LO UNLOCKED!')
                return 'FIS_READY_IFO'
            elif nodes['SQZ_FC'].done and nodes['SQZ_FC'].arrived:
                return True

#-------------------------------------------------------------------------------
class FC_WAIT_FDS(GuardState):
    index = 64
    request = False

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    @LO_checker_FDS
    def main(self):
        nodes['SQZ_FC'] = 'IR_LOCKED'
        self.timer['wait_for_fc'] = 300

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    @LO_checker_FDS
    def run(self):
        if ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'IR_LOCKED':
                return True

        elif self.timer['wait_for_fc']:
            log('FC-IR UNLOCKED!')
            return 'FDS_READY_IFO'

#-------------------------------------------------------------------------------
class FREQ_INDEP_SQZ(GuardState):
    index = 75

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        log('going into FIS')
        if ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) != 'FC_MISALIGNED':
            nodes['SQZ_FC'] = 'FC_MISALIGNED'
        self.timer['wait_for_lo'] = 30

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    def run(self):
        if ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'FC_MISALIGNED':
            notify('FI-SQZ, FC misaligned')
            return True

        if not LO_LOCKED_OMC() and self.timer['wait_for_lo']:
            log('LO UNLOCKED!')
            return 'FIS_READY_IFO'


#-------------------------------------------------------------------------------
class FREQ_DEP_SQZ(GuardState):
    index = 100
    request = True

    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    @IFO_checker
    @LO_checker_FDS
    @FC_checker
    def run(self):
        if use_ifo_asc and ezca['SQZ-ASC_ANG_P_INMON']==0 and ezca['SQZ-ASC_ANG_Y_INMON']==0:
            notify('SQZ ASC AS42 not on?? Please request RESET_SQZ_ASC.')
        
        if use_sqz_angle_adjust and nodes['SQZ_ANG_ADJUST']!= 'ADJUST_SQZ_ANG_ADF':
            notify('SQZ ANG ADJUST servo not on?? Please request SQZ_ANG_SERVO.')

        if not 130 < ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] < 250:
            notify('SQZ angle is out of range. If SQZ looks bad, request RESET_SQZ_ASC_FDS and then FREQ_DEP_SQZ')

        return True


#-------------------------------------------------------------------------------
class NO_SQUEEZING(GuardState):
    """Idle state that does not have any squeezing. This
    should be used when we cannot get stable sqz locking
    and need to get to Observing.
    """
    index = 7
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        turn_off_sqz()
        log('Im in no squeezing')
        if ezca.read('GRD-SQZ_FC_REQUEST_S', as_string=True) == 'IR_LOCKED':
            nodes['SQZ_FC'] = 'IR_FOUND'

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        return True

#-------------------------------------------------------------------------------
def RESET_SQZ_ASC(nodes):
    class RESET_SQZ_ASC(GuardState):
        index = 97
        request = True
        def main(self):
            if ezca.read('GRD-SQZ_ANG_ADJUST_STATE_S', as_string=True) == 'ADJUST_SQZ_ANG_ADF':
                log('Stopping SQZ_ANG_ADJUST')
                nodes['SQZ_ANG_ADJUST'] = 'DOWN'

            log('turning off SQZ AS42 ASC')
            ezca['SQZ-ASC_WFS_SWITCH'] = 0
            log('gracefully clearing SQZ AS42 ASC')

            prev_sus_lock_gains = {}
            for dof in ['POS_P','ANG_P','POS_Y','ANG_Y']:
                ezca.get_LIGOFilter('SQZ-ASC_%s'%(dof)).ramp_gain(0, ramp_time=5, wait=False)
            for sus in ['ZM4', 'ZM5', 'ZM6']:
                for dof in ['P', 'Y']:
                    prev_sus_lock_gains['SUS-%s_M1_LOCK_%s_GAIN'%(sus, dof)] = ezca['SUS-%s_M1_LOCK_%s_GAIN'%(sus, dof)]
                    ezca.get_LIGOFilter('SUS-%s_M1_LOCK_%s'%(sus, dof)).ramp_gain(0, ramp_time=5, wait=False)

            time.sleep(5)
            for dof in ['POS_P','ANG_P','POS_Y','ANG_Y']:
                ezca['SQZ-ASC_%s_RSET'%(dof)] = 2
                ezca.get_LIGOFilter('SQZ-ASC_%s'%(dof)).ramp_gain(1, ramp_time=1, wait=False)
            for sus in ['ZM4', 'ZM5', 'ZM6']:
                for dof in ['P', 'Y']:
                    ezca['SUS-%s_M1_LOCK_%s_RSET'%(sus, dof)] = 2
                    gain = prev_sus_lock_gains['SUS-%s_M1_LOCK_%s_GAIN'%(sus, dof)]
                    ezca.get_LIGOFilter('SUS-%s_M1_LOCK_%s'%(sus, dof)).ramp_gain(gain, ramp_time=2, wait=False)
            log('done')

            if not 150 < ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] < 250:
                log('SQZ angle outside of expected range, it ran away. Resetting to 190deg.')
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = 190
    return RESET_SQZ_ASC
RESET_SQZ_ASC_FDS = RESET_SQZ_ASC(nodes)
RESET_SQZ_ASC_FDS.index = 97


#-------------------------------------------------------------------------------
def SCAN_ALIGNMENT(nodes):
    class SCAN_ALIGNMENT(GuardState):
        index = 105
        request = True
        #DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_ISS', 'SQZ_FC'])


        def calc_local_minimum(self,k):
            derivative = np.polyder(k)
            extremum_arg = np.roots(derivative)
            extremum_val = np.polyval(k,extremum_arg)

            if self.alignment_fom=='asqz':
                local_min = max(extremum_val)
            else: #elif self.alignment_fom=='sqz':
                local_min = min(extremum_val)

            local_min_arg = extremum_arg[list(extremum_val).index(local_min)]
            return local_min, local_min_arg

        def fit(self):
            for ii in range(len(self.data)):
                self.data[ii] = np.array(self.data[ii])

            fig, axs = plt.subplots(1,3,figsize=(13,5))
            SQZANG = self.data[0]
            db = self.data[2]
            OMCQ = self.data[3]
            ofs_sus = {'ZM4':self.data[0]/self.OPTAL['ZM4'+self.dof].GAIN.get(),'ZM6':self.data[1]/self.OPTAL['ZM6'+self.dof].GAIN.get()}
            ofs = ofs_sus['ZM4'] - self.DC['ZM4'+self.dof]

            # fit
            fit_pol = np.polyfit(ofs,10**(db/20),3)
            best_DCPD_ratio, best_ofs = self.calc_local_minimum(fit_pol)

            fit_pol_omcq = np.polyfit(ofs,OMCQ,3)
            best_omcq, best_omcq_ofs = self.calc_local_minimum(fit_pol_omcq)

            best_ofs_ZM4 = self.DC['ZM4'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM4']
            best_ofs_ZM6 = self.DC['ZM6'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM6']


            best_sqz = np.log10(best_DCPD_ratio)*20
            if self.alignment_fom=='asqz':
                min_sqz = max(db)
                min_omcq = max(OMCQ)
            else: #elif self.alignment_fom=='sqz':
                min_sqz = min(db)
                min_omcq = min(OMCQ)

            fit_db = np.log10(np.polyval(fit_pol,ofs))*20
            fit_omcq = np.polyval(fit_pol_omcq,ofs)

            fig.suptitle(self.comdif + ' ' + self.dof)
            axs[0].plot(ofs,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
            axs[0].plot(ofs,fit_db,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz,best_ofs))
            axs[0].set_xlabel('OPTIC ALIGN offset [urad]')
            axs[0].set_ylabel('SQZ [dB]')
            axs[0].grid()
            #axs[0].set_ylim([-7,-1])
            axs[0].legend()

            axs[1].plot(ofs,OMCQ,'*',label='meas: min_omcq=%.2f'%(min_omcq))
            axs[1].plot(ofs,fit_omcq,label='fit: best_omcq=%.2f @ %.2f'%(best_omcq, best_omcq_ofs))
            axs[1].set_xlabel('OPTIC ALIGN offset [urad]')
            axs[1].set_ylabel('OMC_RF3_Q [a.u.]')
            axs[1].grid()
            axs[1].legend()

            mappable = axs[2].scatter(ofs_sus['ZM4'],ofs_sus['ZM6'],c=db)
            axs[2].plot(best_ofs_ZM4,best_ofs_ZM6,'r*',markersize=30)
            fig.colorbar(mappable,ax=axs[1])
            axs[2].set_xlabel('ZM4_%s OPTIC ALIGN [urad]'%(self.dof))
            axs[2].set_ylabel('ZM6_%s OPTIC ALIGN [urad]'%(self.dof))

            filename = '%s_%s_SCAN.png'%(self.comdif,self.dof)
            fig.tight_layout()
            fig.savefig(self.dirname+filename)
            np.savetxt(self.dirname+filename[:-4]+'.txt', np.column_stack(self.data), fmt='%.5f', header=f'ZM4_{self.dof}_OPTIC_ALIGN [urad], ZM6_{self.dof}_OPTIC_ALIGN [urad], sqz [dB], OMC_RF3_Q [a.u.]')
            return best_ofs_ZM4, best_ofs_ZM6

        def fit_SQZANG(self):
            for ii in range(len(self.data)):
                self.data[ii] = np.array(self.data[ii])


            fig, axs = plt.subplots(1,2,figsize=(13,5))
            SQZANG = self.data[0]
            OMCQ = self.data[1]
            db = self.data[2]

            # fit
            fit_pol_SQZANG = np.polyfit(SQZANG,10**(db/20),3)
            fit_pol_OMCQ = np.polyfit(OMCQ,10**(db/20),3)
            best_DCPD_ratio_SQZANG, best_SQZANG = self.calc_local_minimum(fit_pol_SQZANG)
            best_DCPD_ratio_OMCQ, best_OMCQ = self.calc_local_minimum(fit_pol_OMCQ)

            best_sqz_SQZANG = np.log10(best_DCPD_ratio_SQZANG)*20
            best_sqz_OMCQ = np.log10(best_DCPD_ratio_OMCQ)*20

            if self.alignment_fom=='asqz':
                min_sqz = max(db)
            else: #elif self.alignment_fom=='sqz':
                min_sqz = min(db)

            fit_db_SQZANG = np.log10(np.polyval(fit_pol_SQZANG,SQZANG))*20
            fit_db_OMCQ = np.log10(np.polyval(fit_pol_OMCQ,OMCQ))*20

            fig.suptitle('Squeezing angle scan')
            axs[0].plot(SQZANG,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
            axs[0].plot(SQZANG,fit_db_SQZANG,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_SQZANG,best_SQZANG))
            axs[0].set_xlabel('CLF phase [deg]')
            axs[0].set_ylabel('SQZ [dB]')
            axs[0].grid()
            #axs[0].set_ylim([-7,-1])
            axs[0].legend()

            axs[1].plot(OMCQ,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
            axs[1].plot(OMCQ,fit_db_OMCQ,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_OMCQ,best_OMCQ))
            axs[1].set_xlabel('OMCQ')
            axs[1].set_ylabel('SQZ [dB]')
            axs[1].grid()
            axs[1].legend()

            filename = 'SQZANG_SCAN_%d.png'%self.ANGSCAN_idx
            fig.savefig(self.dirname+filename)
            return best_SQZANG, best_OMCQ


        def main(self):
            if ezca['SQZ-ASC_WFS_SWITCH'] ==1:
                turn_off_asc()

            if ezca.read('GRD-SQZ_ANG_ADJUST_STATE_S', as_string=True) == 'ADJUST_SQZ_ANG_ADF':
                nodes['SQZ_ANG_ADJUST'] = 'DOWN'

            self.alignment_fom = 'asqz' #'sqz'
            self.inital_ang = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']

            #self.amp = {'DIF':70,'COM':150}
            self.amp = {'DIF':100,'COM':150}
            self.freq = 1/60
            self.cycle = 1

            self.SQZANG_amp = 30

            self.dof = 'P'
            self.comdif = 'DIF'

            # initialization
            self.direction = {'P':{'DIF':{'ZM4':0.94,'ZM6':-0.34},'COM':{'ZM4':0.34,'ZM6':0.94}},
                              'Y':{'DIF':{'ZM4':0.67,'ZM6':-0.74},'COM':{'ZM4':0.74,'ZM6':0.67}}}
            #self.direction = {'P':{'DIF':{'ZM4':1,'ZM6':0},'COM':{'ZM4':0,'ZM6':1}},
            #                  'Y':{'DIF':{'ZM4':1,'ZM6':0},'COM':{'ZM4':0,'ZM6':1}}}
            self.OPTAL = {}
            self.DC = {}

            self.timer['done'] = 0
            self.counter = 0

            now = datetime.datetime.now()
            self.dirname = '/ligo/www/www/exports/SQZ/GRD/ZM_SCAN/%s/'%now.strftime('%y%m%d%H%M%S')
            #self.dirname = '/data/SQZ/GRD/ZM_SCAN/%s/'%now.strftime('%y%m%d%H%M%S')
            os.mkdir(self.dirname)

            self.ANGSCAN_idx = 0

            self.test = False

            if self.alignment_fom == 'asqz':
                log('Changing to asqz using hard-coded value 100 with negative sign')
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = 100
                ezca['SQZ-CLF_SERVO_IN2POL'] = 1

        #@LO_OMC_checker
        def run(self):
            #bdep_jump = are_dependencies_bad(self.DEPS)
            #if bdep_jump: return bdep_jump

            if ezca.read('GRD-SQZ_MANAGER_REQUEST_S', as_string=True) == 'FREQ_DEP_SQZ':
                return 'FREQ_DEP_SQZ'

            if not self.timer['done']:
                return

            if self.counter == 0:
                #setup and saving intial values
                for sus in ['ZM4','ZM6']:
                    for dof in ['P','Y']:
                        self.OPTAL[sus+dof] = ezca.get_LIGOFilter('SUS-%s_M1_OPTICALIGN_%s'%(sus,dof))
                        self.DC[sus+dof] = self.OPTAL[sus+dof].OFFSET.get()
                        self.OPTAL[sus+dof].TRAMP.put(0.1)
                self.DC['SQZANG'] = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']

                self.scan_start = gpstime.gpsnow() + 1
                self.timer['scan'] = 1/self.freq * self.cycle + 1
                self.timer['done'] = 1
                self.counter += 1
                self.data = [[],[],[]]

            elif self.counter == 1:
                if self.dof == 'P' and self.comdif == 'DIF':
                    # scan SQZANG first time only
                    if self.timer['scan']:
                        self.scan_end = gpstime.gpsnow()
                        self.counter += 1

                    else:
                        now = gpstime.gpsnow()
                        ezca.write('SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                                   self.SQZANG_amp*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC['SQZANG'], log=False)
                        self.data[0].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                        self.data[1].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                        self.data[2].append(ezca['SQZ-DCPD_RATIO_6_DB_MON'])
                        notify('SQZ angle scanning %d sec/%d sec (%d/5)'%(now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+1))
                else:
                    log('not scanning sqz ang after first time')
                    self.counter += 1

            elif self.counter == 2:
                if self.dof == 'P' and self.comdif == 'DIF':
                    # fit scan result
                    notify('fetching SQZ angle scan data')
                    self.ANGSCAN_idx += 1
                    best_ang, best_OMCQ = self.fit_SQZANG()
                    if not self.test:
                        ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = best_ang
                        #ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'] = best_OMCQ
                    self.timer['done'] = 5
                else:
                    log('not scanning sqz ang after first time')
                    self.timer['done'] = 0
                self.counter += 1

            elif self.counter == 3:
                self.scan_start = gpstime.gpsnow() + 1
                self.timer['scan'] = 1/self.freq * self.cycle + 1
                self.counter += 1
                self.data = [[],[],[],[]]

            elif self.counter == 4:
                # scan suspension
                if self.timer['scan']:
                    self.scan_end = gpstime.gpsnow()
                    self.counter += 1

                else:
                    now = gpstime.gpsnow()
                    for sus in ['ZM4','ZM6']:
                        self.OPTAL[sus+self.dof].OFFSET.put(
                            self.direction[self.dof][self.comdif][sus]*self.amp[self.comdif]*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC[sus+self.dof], log=False)

                    self.data[0].append(ezca['SUS-ZM4_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                    self.data[1].append(ezca['SUS-ZM6_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                    self.data[2].append(ezca['SQZ-DCPD_RATIO_6_DB_MON'])
                    self.data[3].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                    notify('%s %s scanning %d sec/%d sec (%d/5)'%(self.comdif,self.dof,now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+2))

            elif self.counter == 5:
                # fit suspension scan result
                notify('fetching %s data'%self.dof)
                zm4_ofs, zm6_ofs = self.fit()
                for sus in ['ZM4','ZM6']:
                    for dof in ['P','Y']:
                        self.OPTAL[sus+dof] = ezca.get_LIGOFilter('SUS-%s_M1_OPTICALIGN_%s'%(sus,dof))
                        self.OPTAL[sus+dof].TRAMP.put(3)
                time.sleep(0.5)
                if not self.test:
                    self.OPTAL['ZM4'+self.dof].OFFSET.put(zm4_ofs)
                    self.OPTAL['ZM6'+self.dof].OFFSET.put(zm6_ofs)
                self.timer['done'] = 5
                self.counter += 1

            elif self.counter == 6:
                if self.dof == 'P':
                    self.dof = 'Y'
                    self.counter = 0
                else:
                    self.counter += 1

            elif self.counter == 7:
                if self.comdif == 'DIF':
                    self.comdif = 'COM'
                    self.dof = 'P'
                    self.counter = 0
                else:
                    self.counter += 1

            elif self.counter == 8:
                if self.alignment_fom == 'asqz':
                    log('Changing back from asqz to SQZ using inital saved value.')
                    ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = self.inital_ang
                    ezca['SQZ-CLF_SERVO_IN2POL'] = 0
                notify('Alignment scan done! Check SQZ ANG via SCAN_SQZANG & go to FREQ_DEP_SQZ.')
                return True
    return SCAN_ALIGNMENT
SCAN_ALIGNMENT_FDS = SCAN_ALIGNMENT(nodes)
SCAN_ALIGNMENT_FDS.index = 105


#-------------------------------------------------------------------------------
def SCAN_SQZANG(nodes):
    class SCAN_SQZANG(GuardState):
        index = 115
        request = True
        #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
        #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])

        def calc_local_minimum(self,k):
            derivative = np.polyder(k)
            extremum_arg = np.roots(derivative)
            extremum_val = np.polyval(k,extremum_arg)

            local_min = min(extremum_val)
            local_min_arg = extremum_arg[list(extremum_val).index(local_min)]
            return local_min, local_min_arg

        def fit_SQZANG(self):
            for ii in range(len(self.data)):
                self.data[ii] = np.array(self.data[ii])


            fig, axs = plt.subplots(1,2,figsize=(13,5))
            SQZANG = self.data[0]
            OMCQ = self.data[1]
            db = self.data[2]

            # fit
            fit_pol_SQZANG = np.polyfit(SQZANG,10**(db/20),3)
            fit_pol_OMCQ = np.polyfit(OMCQ,10**(db/20),3)
            best_DCPD_ratio_SQZANG, best_SQZANG = self.calc_local_minimum(fit_pol_SQZANG)
            best_DCPD_ratio_OMCQ, best_OMCQ = self.calc_local_minimum(fit_pol_OMCQ)

            best_sqz_SQZANG = np.log10(best_DCPD_ratio_SQZANG)*20
            best_sqz_OMCQ = np.log10(best_DCPD_ratio_OMCQ)*20

            min_sqz = min(db)
            fit_db_SQZANG = np.log10(np.polyval(fit_pol_SQZANG,SQZANG))*20
            fit_db_OMCQ = np.log10(np.polyval(fit_pol_OMCQ,OMCQ))*20

            fig.suptitle('Squeezing angle scan')
            axs[0].plot(SQZANG,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
            axs[0].plot(SQZANG,fit_db_SQZANG,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_SQZANG,best_SQZANG))
            axs[0].set_xlabel('CLF phase [deg]')
            axs[0].set_ylabel('SQZ [dB]')
            axs[0].grid()
            #axs[0].set_ylim([-7,-1])
            axs[0].legend()

            axs[1].plot(OMCQ,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
            axs[1].plot(OMCQ,fit_db_OMCQ,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_OMCQ,best_OMCQ))
            axs[1].set_xlabel('OMCQ')
            axs[1].set_ylabel('SQZ [dB]')
            axs[1].grid()
            #axs[1].set_ylim([-7,-1])
            axs[1].legend()

            now = datetime.datetime.now()
            filename = 'SQZANG_SCAN_%s.png'%now.strftime('%y%m%d%H%M%S')
            fig.savefig(self.dirname+filename)
            return best_SQZANG, best_OMCQ


        def main(self):
            if ezca['SQZ-ASC_WFS_SWITCH'] ==1:
                turn_off_asc()

            if ezca.read('GRD-SQZ_ANG_ADJUST_STATE_S', as_string=True) == 'ADJUST_SQZ_ANG_ADF':
                nodes['SQZ_ANG_ADJUST'] = 'DOWN'

            self.freq = 1/120 #1/(300), 1/60
            self.cycle = 1
            self.SQZANG_amp = 30 #100
            self.SQZANG_DC = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']

            self.timer['done'] = 0
            self.counter = 0

            self.dirname = '/ligo/www/www/exports/SQZ/GRD/SQZANG_SCAN/'

        def run(self):
            #bdep_jump = are_dependencies_bad(self.DEPS)
            #if bdep_jump: return bdep_jump

            if not self.timer['done']:
                return

            if self.counter == 0:

                self.scan_start = gpstime.gpsnow() + 1
                self.timer['scan'] = 1/self.freq * self.cycle + 1
                self.timer['done'] = 1
                self.counter += 1
                self.data = [[],[],[]]

            elif self.counter == 1:
                # scan SQZANG
                if self.timer['scan']:
                    self.scan_end = gpstime.gpsnow()
                    self.counter += 1

                else:
                    now = gpstime.gpsnow()
                    ezca.write('SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                               self.SQZANG_amp*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.SQZANG_DC, log=False)
                    self.data[0].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                    self.data[1].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                    #self.data[2].append(ezca['SQZ-DCPD_RATIO_3_DB_MON']) # minimize BLRMS3
                    self.data[2].append(ezca['SQZ-DCPD_RATIO_3_DB_MON']) # minimize BLRMS6
                    notify('SQZ angle scanning %d sec/%d sec'%(now-self.scan_start,1/self.freq))

            elif self.counter == 2:
                # fit scan result
                best_ang, best_OMCQ = self.fit_SQZANG()
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = best_ang
                #ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'] = best_OMCQ
                self.timer['done'] = 2
                self.counter += 1

            elif self.counter == 3:
                return True
    return SCAN_SQZANG

SCAN_SQZANG_FDS = SCAN_SQZANG(nodes)
SCAN_SQZANG_FDS.index = 115

#-------------------------------------------------------------------------------
########## paths for other configurations, (checking NLG, generating alignment beam)  #####


#-------------------------------------------------------------------------------
# Dither lock on seed power for IFO alignment
class LOCKING_SEED_DITHER(GuardState):
    index = 14
    request = False
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        nodes['SQZ_OPO_LR'] = 'LOCKED_SEED_DITHER'
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO_LR'].arrived and nodes['SQZ_OPO_LR'].done:
            log('OPO dither locked on SEED.')
            return True

class LOCKED_SEED_DITHER(GuardState):
    index = 17
    request = False
    @ISC_library.unstall_nodes(nodes)
    def run(self):
       if ezca.read('GRD-SQZ_OPO_LR_STATE_S', as_string=True) == 'LOCKED_SEED_DITHER':
           return True
       else:
           return 'LOCKING_SEED_DITHER'



#-------------------------------------------------------------------------------
# Measure NLG, Lock OPO on GREEN & IR co-resonance
class LOCKING_SEED_NLG(GuardState):
    index = 24
    request = False
    @SHG_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        nodes['SQZ_LO_LR']  = 'DOWN'
        nodes['SQZ_CLF_LR'] = 'DOWN'
        nodes['SQZ_OPO_LR'] = 'LOCKED_SEED_NLG'
        nodes['SQZ_FC'] = 'FC_MISALIGNED'
        self.counter = 0

    @SHG_checker
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if self.counter==0:
            if nodes['SQZ_OPO_LR'].arrived and nodes['SQZ_OPO_LR'].done:
                log('Measure NLG, OPO is locked on dual green + ir resonance.')
                if nodes['SQZ_FC'].arrived and nodes['SQZ_FC'].done:
                    log('FC misaligned')
                    self.counter += 1
        if self.counter==1:
            ezca['SQZ-SEED_PZT_SCAN_ENABLE'] = 1
            return True

class LOCKED_SEED_NLG(GuardState):
    index = 27
    request = True
    @SHG_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        nodes['SQZ_CLF_LR'] = 'DOWN'
    def run(self):
       if not ezca['SYS-MOTION_C_BDIV_E_POSITION']:
           notify('bdiv open but trying to measure NLG?')
           ezca['SQZ-SEED_FLIPPER_CONTROL']=0
           return False
       if ezca.read('GRD-SQZ_OPO_LR_STATE_S', as_string=True) == 'LOCKED_SEED_NLG':
           if ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'FC_MISALIGNED':
               return True
       #else:
       #    return 'LOCKING_SEED_NLG'



#-------------------------------------------------------------------------------
class SQZ_READY_HD(GuardState):
    index = 40
    request = False
    @SHG_checker
    @OPO_checker
    @TTFSS_checker
    @ISC_library.unstall_nodes(nodes)
    def main(self):
        ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1

        #if not ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'FC_MISALIGNED':
        #    nodes['SQZ_FC'] = 'IR_FOUND'
        nodes['SQZ_LO_LR'] = 'LOCKED_HD'
        #notify('clearing SQZ ASC offsets')
        #for dof in ['POS_P', 'ANG_P','POS_Y', 'ANG_Y']:
        #    ezca['SQZ-ASC_%s_RSET'%(dof)] = 2
        '''
        #ezca['SUS-FC1_M1_OPTICALIGN_P_OFFSET'] = 243.5
        #ezca['SUS-FC1_M1_OPTICALIGN_Y_OFFSET'] = -108.2
        ezca['SUS-FC1_M1_OPTICALIGN_P_OFFSET'] = 222.5 #4/17
        ezca['SUS-FC1_M1_OPTICALIGN_Y_OFFSET'] = -112.4 #4/17
        '''
        notify('align FC1 P/Y for HD, and Set ZMs HD')

    @TTFSS_checker
    @SHG_checker
    @OPO_checker
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            nodes['SQZ_OPO_LR'] = 'DOWN'
            nodes['SQZ_CLF_LR'] = 'DOWN'
            nodes['SQZ_LO_LR'] = 'DOWN'
            return 'LOCK_FC'

        if ezca.read('GRD-SQZ_LO_LR_STATE_S', as_string=True) != 'LOCKED_HD':
            notify('LO not locked') #no need to do anything. LO will relock itself
            return False

        elif not FC_LOCKED():
            notify('FC swinging around')
            return False

        else:
            return True



#-------------------------------------------------------------------------------
########## Set ZM 4/5/6 sliders for either IFO or HD SQZ ###############################

# Offload FC and SQZ ASC to FC1/2, ZM3/5/6
class OFFLOAD_FC_ASC(GuardState):
    index = 31
    request = False

    def main(self):
        nodes['SQZ_FC'] = 'FC_ASC_OFFLOADED' #offload FC ASC
        return True


#-------------------------------------------------------------------------------

#####  OFFLOAD SQZ-ASC TO ZM 5/6 SLIDERS  #####
OFFLOAD_SQZ_ASC = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('SQZ_ASC', 5, ['ZM4','ZM5', 'ZM6'])
OFFLOAD_SQZ_ASC.index = 45

# clear SQZ-ASC history after offloading sliders
class CLEAR_SQZ_ASC(GuardState):
    index = 46
    request = False
    def main(self):
        log('clearing SQZ AS42 ASC histories')
        for dof in ['POS_P','ANG_P','POS_Y','ANG_Y']:
            ezca['SQZ-ASC_%s_RSET'%(dof)] = 2
        return True


#class SQZ_ASC_OFFLOADED(GuardState):
#    index = 47
#    request = False
#    def run(self):
#        return True


##################################################
edges = [
    ('INIT', 'DOWN'),
    ('DOWN', 'NO_SQUEEZING'),
    ('DOWN', 'LOCK_TTFSS'),
    ('LOCK_TTFSS','LOCK_PMC'),
    ('LOCK_PMC','LOCK_SHG'),
    ('LOCK_SHG', 'LOCK_OPO'),
    ('LOCK_OPO', 'LOCK_CLF'),
    ('LOCK_CLF',  'FIS_READY_IFO'),
    ('LOCK_CLF',    'LOCK_FC'),
    ('LOCK_FC', 'FDS_READY_IFO'),
    ('FDS_READY_IFO', 'FDS_READY_IFO'),
    ('FDS_READY_IFO', 'NO_SQUEEZING'),
    ('FDS_READY_IFO', 'BEAMDIV_OPEN_FDS'),
    ('BEAMDIV_OPEN_FDS','LOCK_LO_FDS'),
    ('LOCK_LO_FDS','FC_WAIT_FDS'),
    ('FIS_READY_IFO', 'BEAMDIV_OPEN_FIS'),
    ('BEAMDIV_OPEN_FIS','LOCK_LO_FIS'),
    ('LOCK_LO_FIS','SQZ_ASC_FIS'),
    ('SQZ_ASC_FIS','FREQ_INDEP_SQZ',2),
    ('FC_WAIT_FDS','SQZ_ASC_FDS'),
    ('FREQ_INDEP_SQZ', 'FC_WAIT_FDS', 2),
    ('FREQ_DEP_SQZ',    'MISALIGN_FC'),
    ('MISALIGN_FC',     'FREQ_INDEP_SQZ'),
    ('FREQ_DEP_SQZ',    'NO_SQUEEZING'),
    ('FREQ_INDEP_SQZ',  'NO_SQUEEZING'),
    ('FREQ_DEP_SQZ',    'RESET_SQZ_ASC_FDS'),
    ('RESET_SQZ_ASC_FDS', 'SQZ_ASC_FDS'),
    ('FREQ_DEP_SQZ',    'SCAN_ALIGNMENT_FDS'),
    ('SCAN_ALIGNMENT_FDS', 'SCAN_SQZANG_FDS'),
    ('SQZ_ASC_FDS', 'SQZ_ANG_SERVO'),
    ('SQZ_ANG_SERVO', 'FREQ_DEP_SQZ'),
    ('FREQ_DEP_SQZ',    'SCAN_SQZANG_FDS'),
    ('SCAN_SQZANG_FDS', 'SQZ_ASC_FDS'),


    ('LOCK_CLF', 'SQZ_READY_HD', 10),

    # manager for zm's?
    #('FREQ_DEP_SQZ', 'OFFLOAD_SQZ_ASC'),   #offload SQZ ASC (ZM5/6) in FDS w/ASC running
    #('OFFLOAD_SQZ_ASC', 'FREQ_DEP_SQZ'),   #offload SQZ ASC (ZM5/6) in FDS w/ASC running
    ('LOCK_SHG','LOCKING_SEED_NLG'),
    ('LOCKING_SEED_NLG', 'LOCKED_SEED_NLG'),
    ('DOWN', 'LOCKING_SEED_DITHER'),
    ('LOCKING_SEED_DITHER', 'LOCKED_SEED_DITHER'),

    ('DOWN','OFFLOAD_SQZ_ASC'),  #offload SQZ ASC (ZM5/6)
    ('OFFLOAD_SQZ_ASC', 'CLEAR_SQZ_ASC'),  #offload SQZ ASC (ZM5/6)
    ('CLEAR_SQZ_ASC', 'OFFLOAD_FC_ASC'),

]
